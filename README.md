# Streamlike Media

This module provides a "Streamlike Media" field type, which allows easy display of a Streamlike media just with providing its id.

Streamlike is a carbon neutral SaaS Enterprise Video Platform for live and on-demand streaming: [https://www.streamlike.com](https://www.streamlike.com)
