<?php

namespace Drupal\streamlike_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Streamlike Media' formatter.
 *
 * @FieldFormatter(
 *   id = "streamlike_media_field_formatter",
 *   label = @Translation("Streamlike Media"),
 *   field_types = {
 *      "streamlike_media_field"
 *   }
 * )
 * 
 */
class StreamlikeMediaFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'display_player' => "1",
      'display_player_width' => "650",
      'display_player_height' => "366",
      'display_player_cdn' => "",
      'display_link' => "0",
      'display_link_blank' => "0",
      'display_link_prefix' => t("Link to media").":",
      'display_link_suffix' => "",
      'display_id' => "0",
      'display_id_prefix' => t("Media ID").":",
      'display_id_suffix' => "",
    ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Display Player settings
    $form['display_player'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Streamlike player'),
      '#default_value' => $this->getSetting('display_player'),
    ];    
    $form['display_player_width'] = [
      '#type' => 'textfield',
      "#size" => 6,
      '#title' => $this->t('Player width'),
      '#default_value' => $this->getSetting('display_player_width'),
    ];
    $form['display_player_height'] = [
      '#type' => 'textfield',
      "#size" => 6,
      '#title' => $this->t('Player height'),
      '#default_value' => $this->getSetting('display_player_height'),
    ];
    $form['display_player_cdn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streamlike CDN to use'),
      '#default_value' => empty($this->getSetting('display_player_cdn')) ? $this->getFieldSetting('cdn_default') : $this->getSetting('display_player_cdn'),
    ];
    $form['display_player_separator'] = [ '#markup' => "<hr noshade size=\"1\">" ];

    // Display Link settings
    $form['display_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display link to Streamlike Media'),
      '#default_value' => $this->getSetting('display_link'),
    ];   
    $form['display_link_blank'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open in new window'),
      '#default_value' => $this->getSetting('display_link_blank'),
    ];  
    $form['display_link_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix text'),
      '#default_value' => $this->getSetting('display_link_prefix'),
    ];  
    $form['display_link_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix text'),
      '#default_value' => $this->getSetting('display_link_suffix'),
    ];
    $form['display_link_separator'] = [ '#markup' => "<hr noshade size=\"1\">" ];

    // Display ID settings
    $form['display_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Streamlike Media ID'),
      '#default_value' => $this->getSetting('display_id'),
    ];   
    $form['display_id_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix text'),
      '#default_value' => $this->getSetting('display_id_prefix'),
    ];  
    $form['display_id_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix text'),
      '#default_value' => $this->getSetting('display_id_suffix'),
    ]; 

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $display = [];
    if ( $this->getSetting('display_player') ) $display[] = $this->t("Player");
    if ( $this->getSetting('display_link') ) $display[] = $this->t("Link to media");
    if ( $this->getSetting('display_id') ) $display[] = $this->t("Media id");
    if ( empty($display) ) $display[] = $this->t("None");
    $summary[] = $this->t('Display mode: @display', ['@display' => implode(", ",$display)]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // Get Streamlike CDN
    $cdn = empty($this->getSetting('display_player_cdn')) ? $this->getFieldSetting('cdn_default') : $this->getSetting('display_player_cdn');
    if ( empty($cdn) ) $cdn = "cdn.streamlike.com";

    // Process field items
    foreach ($items as $delta => $item) {//ksm($item);

      // Build display_player render array
      if ( $this->getSetting('display_player')=="1" ) {

        // Get width as value and unit
        $width = $this->parseDimension($this->getSetting('display_player_width'));
        if ( !$width ) {
          $width['value'] = "650";
          $width['unit'] = "px";
        }
        if ( empty($width['unit']) ) $width['unit'] = "px";

        // Get height as value and unit
        $height = $this->parseDimension($this->getSetting('display_player_height'));
        if ( !$height ) {
          $height['value'] = "366";
          $height['unit'] = "px";
        }
        if ( empty($height['unit']) ) $height['unit'] = "px";

        // Compute ratio
        $ratio = intval($height['value'])/intval($width['value'])*100;

        // Build the responsive iframe inline css
        $inline_css = "
        .streamlike-player-wrapper {
          width: 100%;
          max-width: ".$width['value'].$width['unit'].";
          max-height: ".$height['value'].$height['unit'].";
        }
        .streamlike-player-container {
          position: relative;
          overflow: hidden;
          width: 100%;
          padding-top: ".round($ratio,2)."%; 
        }
        .streamlike-player-responsive-iframe {
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 100%;
          max-width: ".$width['value'].$width['unit'].";
          max-height: ".$height['value'].$height['unit'].";
        }
        ";

        // Compose the player iframe markup
        $display['display_player'] = [
          '#type' => 'inline_template',
          '#template' => '
          <style>{{ inline_css }}</style>
          <div class="streamlike-player-wrapper">
            <div class="streamlike-player-container display-player">
              <iframe 
                class="streamlike-player-responsive-iframe" 
                src="https://{{ cdn }}/play?med_id={{ med_id }}" 
                style="border:0px;" 
                name="media-{{ med_id }}" 
                marginheight="0" 
                marginwidth="0" 
                scrolling="no" 
                frameborder="0" 
                align="top" 
                {{ display_player_width }}{{ display_player_height }} 
                mozallowfullscreen 
                webkitallowfullscreen 
                allowfullscreen 
                allow="autoplay; fullscreen"
              ></iframe>
            </div>
          </div>
          ',
          '#context' => [
            'inline_css' => $inline_css,
            'cdn' => $cdn,
            'med_id' => $item->value,
            'display_player_width' => $width['unit']=="px" ? "width=".$width['value']." " : "width=\"100%\" ",
            'display_player_height' => $height['unit']=="px" ? "height=".$height['value']." " : "height=\"100%\" ",
          ],
        ];
      } 
      
      // Build display_link render array
      if ( $this->getSetting('display_link')=="1" ) {

        // Get prefix and suffix texts
        $display_link_prefix = empty($this->getSetting('display_link_prefix')) ? "" : $this->getSetting('display_link_prefix');
        $display_link_suffix = empty($this->getSetting('display_link_suffix')) ? "" : $this->getSetting('display_link_suffix');

        // Build link
        $url = "https://".$cdn."/play?med_id=".$item->value;

        // Build field template conditionally
        $link_template = [];
        $link_template[] = "<div class=\"display-link\">";
        $link_template[] = empty($display_link_prefix) ? "" : "<span class=\"display-prefix display-link-prefix\">{{ display_link_prefix }}</span> ";
        $link_template[] = "<a href=\"{{ display_url }}\" class=\"display-link\" target=\"{{ display_link_target }}\">{{ display_url }}</a>";
        $link_template[] = empty($display_link_suffix) ? "" : " <span class=\"display-suffix display-link-suffix\">{{ display_link_suffix }}</span>";
        $link_template[] = "</div>";

        // Compose inline template
        $display['display_link'] = [
          '#type' => 'inline_template',
          '#template' => implode("",$link_template),
          '#context' => [
            'display_url' => $url,
            'display_link_target' => empty($this->getSetting('display_link_blank')) ? "_self" : "_blank",
            'display_link_prefix' => $display_link_prefix,
            'display_link_suffix' => $display_link_suffix,
          ],
        ];

      }

      // Build display_id render array
      if ( $this->getSetting('display_id')=="1" ) {

        // Get prefix and suffix texts
        $display_id_prefix = empty($this->getSetting('display_id_prefix')) ? "" : $this->getSetting('display_id_prefix');
        $display_id_suffix = empty($this->getSetting('display_id_suffix')) ? "" : $this->getSetting('display_id_suffix');

        // Build field template conditionally
        $id_template = [];
        $id_template[] = "<div class=\"display-id\">";
        $id_template[] = empty($display_id_prefix) ? "" : "<span class=\"display-prefix display-id-prefix\">{{ display_id_prefix }}</span> ";
        $id_template[] = "<span class=\"display-id-value\">{{ display_id }}</span>";
        $id_template[] = empty($display_id_suffix) ? "" : " <span class=\"display-suffix display-id-suffix\">{{ display_id_suffix }}</span>";
        $id_template[] = "</div>";

        // Compose inline template
        $display['display_id'] = [
          '#type' => 'inline_template',
          '#template' => implode("",$id_template),
          '#context' => [
            'display_id' => $item->value,
            'display_id_prefix' => $display_id_prefix,
            'display_id_suffix' => $display_id_suffix,
          ],
        ];

      }

      // Return render array
      $element[$delta] = empty($display) ? [] : $display;

    }

    return $element;

  }

  /**
   * Extracts the value and unit from a dimension string
   *
   * @param String $dimension
   *  The dimension to extract value and unit, eg 100px, 50%
   * 
   * @return Array
   *  A keyed array with keys: value, unit
   */
  public function parseDimension($dimension) {

    // Match the value and the unit in the dimension string
    preg_match('/^([\d,\.]+)\s*([a-z%]*)$/i', trim($dimension), $matches);
    
    // If regex matches, return value/unit array
    if (count($matches) == 3) {
      $value = str_replace(',', '.', $matches[1]);
      $unit = $matches[2];
      return array('value' => $value, 'unit' => $unit);
    } else {
      // If regex doesn't match, return null
      return null;
    }

  }

}
