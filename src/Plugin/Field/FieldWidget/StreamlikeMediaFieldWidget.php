<?php

namespace Drupal\streamlike_media\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'streamlike_media_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "streamlike_media_field_widget",
 *   label = @Translation("Streamlike Media"),
 *   field_types = {"streamlike_media_field"},
 * )
 */
class StreamlikeMediaFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
    ];

    return $element;
  }

  
}
